#include <stdio.h>
#include <malloc.h>

#define SIZE 1024

static void* globalptr;

void dump_ints(void* where, int count) {
    printf ("dump ints [%ld] (%d)\n", where, count);
	while (count > 0) {
		printf ("%d ", *((int*)(where)));
		count -= 1;
		where += 4;
	}
	printf("\n");
}

void byte_map(void* where, int x, int y) {
    int i, j, b;
    for (i = 0; i < y; i++) {
        for (j = 0; j < x; j++) {
            if (*((char*)(where)) > 0) {
                printf("%s ", "#");
            } else {
                printf("%s ", "_");
            }
            where++;
        }
        printf("\n");
    }
}

int read_firsti(void *addr) {
    int result = 0;
    int i;
    void* ptr = addr;
    int b = *((int*)(ptr));
    result  = b;
    //printf("firsti [%ld] = %d\n", (long)addr, result);
    return result;
}

int read_secondi(void *addr) {
    void* ptr = addr + 4;
    int result = 0;
    int b = *((int*)(ptr));
    result  = b;

    //printf("secondi [%ld] = %d\n", (long)addr, result);
    return result;
}

void write_secondi(void *addr, int data) {
	printf ("> write_secondi (%ld, %d)\n", (long)addr, data);
    void* ptr = addr + 4;
    int result = data;
        *((int*)(ptr)) = data;
        dump_ints(addr, 10);
    printf("secondi [%ld] = %d written\n", (long)addr, data);
}

void write_firsti(void *addr, int data) {
	printf ("> write_firsti (%ld, %d)\n", (long)addr, data);
    void* ptr = addr;
    int result = data;
        *((int*)(ptr)) = data;
        dump_ints(addr, 10);
    printf("firsti [%ld] = %d written\n", (long)addr, data);
}

void allocate_here(void* where, size_t size, int size_of_next, char bit) {
    printf("> allocate_here(%ld, %ld, %ld)\n", (long)where, size, size_of_next);
    write_firsti(where, (int)size);
    dump_ints(where, 10);
    if (bit > 0)
        *((int*)(where)) =  -*((int*)(where));
    
    dump_ints(where, 10);
    write_secondi(where, (int)size_of_next);
    dump_ints(where, 10);
    printf("< allocate_here(%ld, %ld, %ld)\n", (long)where, size, size_of_next);
}

void allocate_in(void* where, size_t size, void* prev_pointer) {
    printf("> allocate in (%ld, %ld)\n", where, size);
    printf("size is %ld\n", size);
    int thisword = read_firsti(where) >= 0 ? read_firsti(where) : -read_firsti(where);
    int nextword = read_secondi(where);
    printf("thisword is %d and secondword is %d\n", thisword, nextword);
    size_t realsize = size + 8; //add size of header
    printf("realsize is %ld (mod 4 = %d)\n", realsize, realsize % 4);
    realsize += 4 - (realsize % 4); //align by 4
    printf("aligned realsize is %ld\n", realsize);
    int size_of_next = (thisword - (int)realsize);
    printf("realsize of next is %d\n", size_of_next);
    allocate_here(where, realsize, size_of_next, 1);
    allocate_here(where + realsize, size_of_next, nextword, 0);
    
    //update previous section 
    allocate_here(prev_pointer, read_firsti(prev_pointer), realsize, 0);
    
    printf("< allocate in (%ld, %ld)\n", where, size);
    byte_map(where, 4, 16);
}

void* init(int size) {
    printf ("initialization of %d\n", size);
    globalptr = malloc(size);
    printf ("memset...");
    dump_ints(globalptr, 8);
    memset(globalptr, 0, size);
    printf ("done\n");
    //allocate_here(globalptr, size - 8, 0, 0);

    //write it manually
    write_firsti(globalptr, size);
    write_secondi(globalptr, 0);

    dump_ints(globalptr, 10);

    read_firsti(globalptr);
    read_secondi(globalptr);

    printf ("init ends with:\n");
    dump_ints(globalptr, 10);
    return globalptr;
}

void* mem_alloc(size_t size) {
    printf("> mem_alloc(size = %ld)\n", (long)size);
    int prev = 0;
    int localptr = 0;
    if (read_firsti(globalptr) == 0) {
        printf("This space (at %ld) is not allocated, exiting...\n", globalptr);
        return NULL;
    }
    while (/*localptr + size < SIZE */ 1) {
        if ( *((int*)(globalptr+localptr)) < 0) {
            printf("not empty place detected at %ld (%ld + %ld)\n", globalptr+localptr, globalptr, localptr);
            prev = localptr;
            localptr += -read_firsti(globalptr+localptr);
            printf("moving next by %d, now %ld (%ld + %ld) \n", globalptr+localptr, globalptr, localptr);
            continue;
        }
        
        int firsti = read_firsti(globalptr+localptr) -1;
        if (firsti >= size) {
            allocate_in(globalptr+localptr, size, globalptr+prev);
            printf("allocated %ld bytes  at %ld (%ld + %ld)\n", size, globalptr+localptr, globalptr, localptr);
            return globalptr + localptr;
        } else {
            printf("not enough space [%d] at %ld (%ld + %ld)\n%ld needed\n", firsti, globalptr+localptr, globalptr, localptr, size);
            if (read_secondi(globalptr+localptr) == 0) {
                printf("and size of next is 0\n");
                break;
            }
            localptr += read_firsti(globalptr+localptr);
            continue;
        }
    }
    printf("< mem_alloc(size = %ld) = NULL\n", (long)size);
    return NULL;
}

void* mem_realloc(void *addr, size_t size) {
    
}

void* mem_free(void *addr) {
    
}

int main () {
    printf ("hello, world!\n");
    globalptr = init(SIZE);
    printf ("and here init ends with:\n");
    dump_ints(globalptr, 10);
    //test_mem(globalptr);
    //printf ("this is malloc %ld please\n", (long)globalptr);
    //printf ("sizeof (long) = %ld \n", sizeof(long));
    //printf ("sizeof (int) = %ld \n", sizeof(int));
    //printf ("sizeof (size_t) = %ld \n", sizeof(size_t));
    void* pointer = mem_alloc((size_t)10);
    dump_ints(globalptr, 80);
    void* pointer2 = mem_alloc((size_t)40);
    dump_ints(globalptr, 80);
    void* pointer3 = mem_alloc((size_t)451);
    dump_ints(globalptr, 80);
    byte_map(globalptr, 4, 32);
    return 0;
}
